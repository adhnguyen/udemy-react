import React, { Component } from 'react';
import '../App.css';

class Gallery extends Component {
    constructor(props) {
        super(props);

        this.state = {
            playing: false,
            audio : null,
            playingUrl : '',
        }
    }

    playAudio(url) {
        let audio = new Audio(url);
        
        if (!this.state.playing) {
            audio.play();
            this.setState({
                playing: true,
                playingUrl: url,
                audio
            })
        } else {
            this.state.audio.pause();
            if (this.state.playingUrl === url) {
                //pause the current
                this.setState({
                    playing: false 
                })
            } else {
                audio.play()
                //start the new song
                this.setState({
                    playing : true,
                    playingUrl : url,
                    audio    
                })

            }
        }
    }

    render() {  
        const { tracks } = this.props;
        
        return (
            <div className="gallery">
                {tracks.map((track, k) => {
                    console.log(track);
                    return (
                        <div 
                            className="track" 
                            key={k}
                            onClick={() => this.playAudio(track.preview_url)}
                        >
                            <img 
                                className="track-img" 
                                src={track.album.images[0].url}
                                alt="track"
                            />
                            
                            <div className="track-name">
                                {track.name}
                            </div>
                        </div>
                    )
                })}    
            </div>
        )
    }
}

export default Gallery;